[![pipeline status](https://gitlab.com/untztime/untztime-website/badges/master/pipeline.svg)](https://gitlab.com/untztime/untztime-website/-/commits/master)

# UntzTime Website

The UntzTime website. https://untztime.com

## Project Dependencies

This project requires these dependencies to be installed and running:

- Ruby 3.x

## Getting Started

1. Clone the repository.

```shell
git clone git@gitlab.com:untztime/untztime-website.git && cd untztime-website
```

2. Install `jekyll` gem.

```
gem install jekyll
```

3. Run the local server.

```
jekyll serve
```

3. View the website at http://localhost:4000

## Contributing

### Formatting

This project uses [Prettier](https://prettier.io) for formatting. Add a hook in your editor of choice to
run it after a save.

### Commits

Git commit subjects use the [Karma style](http://karma-runner.github.io/5.0/dev/git-commit-msg.html).
